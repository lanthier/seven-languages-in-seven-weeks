### Seven Languages in Seven Weeks
--
This repository holds all of my lab work for this book. However I will likely do a new language every other week, instead of every week.

![Book Cover](repo-assets/book-cover.jpg)