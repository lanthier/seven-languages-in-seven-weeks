# Print contents of an array of sixteen numbers, using each
array = Array.new(16) { |e| e = rand(10) }
array.each {
    |x| print x, ", "
}

# Print contents of an array of sixteen numbers, using each_slice
array.each_slice(1) {
    |x| puts x
}

#Specify new tree with clean interface
class Tree
    attr_accessor :children, :node_name

    def initialize(tree = {})
        @node_name = tree.keys[0]
        @children = tree[@node_name].collect{ |k, v| Tree.new({k => v}) }
    end

    def visit_all(&block)
        visit &block
        children.each {|c| c.visit_all &block}
    end

    def visit(&block)
        block.call self
    end
end

tree = Tree.new(:grandpa => {:dad => {:child1 => {}, :child2 => {}}, :aunt => {:child3 => {}}})

tree.visit_all { |n| puts n.node_name }

# Print the lines of a file where a specific phrase is found
def grep(filename, phrase)
  File.open(filename) do |f|
    f.each { |line| puts "#{f.lineno}: #{line}" if line[phrase] }
  end
end

grep("test-file.txt", "peru")
grep("test-file.txt", "hello")