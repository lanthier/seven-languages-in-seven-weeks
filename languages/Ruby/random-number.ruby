puts "Welcome to the random number game, guess a number!"

randomNumber = rand(10)

loop do
    guess = gets.to_i
    
    if guess > randomNumber
        puts "You have guessed too high! Guess again"

    elsif (guess < randomNumber)
        puts "You have guessed too low! Guess again"

    else
        puts "You are correct, the answer is #{randomNumber}."
        break
    end
end