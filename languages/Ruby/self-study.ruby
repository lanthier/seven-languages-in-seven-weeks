# Print Hello, World
puts "Hello World!"

# For the string "Hello, Ruby", find the index of the word "Ruby"
puts "Hello, Ruby,".index("Ruby")

# Print your name ten times [Static number loop]
10.times { 
    puts "Alex"
}

# Print the string "This is sentence number 1" where number 1 changes from 1 to 10 [Range loop]
(1..10).each {
    |i| puts "This is sentence number #{i}"
}

#Run a Ruby program from a file
# ^ Just run  ruby *.ruby

# --- My own self study ---

# Check how a variable is defined
x = 10
puts defined? x

$y = 11
puts defined? $y

# What is false?

if false
    puts "false is true"
end

if 0
    puts "0 is true"
end