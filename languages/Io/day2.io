# Doing my own version of these excercies for Io
# Recursion and memoization excercize with the fibinocci sequence
# Any sane person would just do this in a loop

# ---Helpers---
allocate := method(
    aNumber,
    for(i, 0, aNumber, self append(nil))
)
# ---/Helpers---

#This algorithm is specifically Omega(1.41^n) and O(2^n) which is horrible, but is standard recursion
recfib := method( 
    n, 
    if( n < 2, n, recfib(n-1) + recfib(n-2) ) 
)

#This algorithm uses a dynamic programming concept called "memoization" and is linear, O(n)

fibCache := List clone;
fibCache allocate(81)

memoizedFib := method(
    n,
    if(n < 2, return n)

    if(fibCache at(n) == nil, fibCache atPut(n, ( memoizedFib(n-1) + memoizedFib(n-2) ) ) )
    
    return fibCache at(n)
)


memoizedFib(30) println

recfib(30) println