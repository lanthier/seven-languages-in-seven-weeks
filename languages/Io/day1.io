# --- Helpers ---
printSeperator := method("----" println)
# --- /Helpers ---


1 + 1 println

printSeperator

e := try(1 + "one")

e catch(Exception,
  "Io is strongly typed, the 1 is expecting a type Number" println
)

printSeperator

#Check if 0 is true or false, it is true
("0 is " .. if(0 or false) )println

#Check if nil is true or false, it is false
("nil is " .. if(nil or false) ) println

#Empty string true or false, it is true
("Empty string value '' is " .. if("" or false) ) println

printSeperator

#Print the names of each slot

#Create custom object
Vehicle := Object clone
Vehicle modeOfTransport := "road"
Vehicle color := "green"
Vehicle weight := 3000

#For loop: variable to declare, start value, value to iterate to, and code to run for each iteration
for(i, 0, (Vehicle slotNames size) - 1, Vehicle slotNames at(i) println);

printSeperator

#Execute code in a slot given it's name

Vehicle selfDestruct := method("boom" println) 

Vehicle callMethodByName := method(name, self getSlot(name) call);

Vehicle callMethodByName("selfDestruct")
